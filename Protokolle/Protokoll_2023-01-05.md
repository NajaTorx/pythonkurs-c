# Pikos Python Kurs am 05.01.2023

Die Lösungen zu den letzten Hausaufgaben gibt es als Videos. Sie werden in den kommenden Tagen hier veröffentlicht:
https://diode.zone/c/pykurs_c/videos

## Links
https://media.ccc.de

#genuary https://genuary.art/prompts

## Protokoll
Protokoll 05.01.2023

### Was random macht

- ist ein Modul (Legokiste): Zusammenstellung verschiedener Funktionalitäten
- muss importiert werden: import random → dann Zugriff auf die verschiedenen Funktionen
- modules sind Sachen, die man schon hat und nur noch importieren muss
- diese Funktion macht etwas:

__________

Exkurs
- manche Funktionen können etwas auffangen, aus anderen fällt etwas herunter, wie Orangensaftpressautomaten i m Supermarkt
- „zahl“ ist der Behälter, der frischgepresse Orangensaft muss da rein, also muss der Behälter hin
- andere Funktionen eher wie ein Solarium: man geht rein, kommt anders wieder raus, aber gewinnt außer dieser Änderung nichts

__________

- import random
zahl = random.random()
print (zahl)

→ mehrere Male ausführen lassen
→ anhand der Ergebnisse Vermutung aufstellen: es kommt eine Kommazahl zwischen 0 und 1 raus

- anderes Ergebnis bei ranint (zahl = random.randint(1, 6) → random integer zwischen 1 und 6, 1 und 6 inbegriffen (bei range wäre die letzte Zahl nicht dabei)

- statt einer Zahl könnte etwas anderes in die Variable genommen werden, oder es könnte manipuliert werden, z.B. mit print(zahl*4) → Ergebnis wird dann immer mit 5 multipilziert


HIER EINE FRAGE AUS DEM CHAT + ERKLÄRUNG VERPASST

Warum braucht random.choice Doppelklammern?

→ random.choice will eine Liste haben
→ bei random.choice(liste) sind diese Klammern ja schon dabei
→ wenn man alles einzeln eingibt aber in runden Klammern, geht es nicht, weil dann nur das erste als Liste interpretiert wird, die nächsten Punkte nicht, da die Position der Argumente wichtig ist
→ das Erste muss eine Auflistung sein, aus der etwas gewählt werden kann, das Zweite hat auch eine bestimmte Funktion, danach darf nichts mehr kommen


liste = [„Apfel, „Tomate“, „Mozartkugel“, „Kekse“, „Limonade“]
suche = random.choice(liste)


### Hausaufgaben: Regenbogen 
(https://md.ha.si/AO2e_MfpSSW033tLJ8cGKg#)

- viele Einstellungen für turtle 
- Lange Liste
- Vorschleife: Farbe wird eingestellt, Kreis gemacht, am Ende wird der Stift wieder abgestellt


Ergänzungen dazu:

Namen der Variablen
- i für index: zählt von 0 nach oben, kann also einfach genau so verwendet werden, macht eigenen und fremden Code einfacher zu lesen (ist etabliert, es einfach als i zu nutzen)

Was ist ein sinnvoller Name für b und was müssten wir daran noch verändern?

- Farben oder Farbenlisten

→ dann können wir sagen: for farbe in farbenliste: 
→ damit ist die Vorschleife sofort viel einfacher zu verstehen

_____________
Exkurs:

Ist farbe implizit initialisiert? 
- ja
- bei anderen Programmiersprachen muss man erstmal z.B. i erschaffen und festlegen, was es ist und was drin ist, „erschaffe i, tu 100 in i rein“

_____________

dritte Variable: x
- x-Koordinate der turtle ist am Anfang 0
- Turtle geht dann zu 0, wird dann größer, dann springt die turtle immer weiter zur Seite
- y bleibt 0


→ Variablennamen super wichtig


Whitespace – was man nicht sieht

- nach den imports, wenn es mehrere sind, macht man danach eine Leerzeile
- in den einzelnen Zeilen: z.B. i LEERZEICHEN = LEERZEICHEN i LEERZEICHEN + LEERZEICHEN dicke
- auch in Klammern, zieht Klammern und Zeilen auseinander
- auch nach jedem Komma
- macht Code besser lesbar, ist stilistisch aber hilft zur Orientierung im eigenen und fremden Code


Übung: was würden wir am Code verändern? 
- Leerzeilen nach import und vor „for i in farben“
- ggf. innerhalb der Vorschleife noch mal, um thematisch zu unterscheiden zwischen Farbkreis und penup() bis pendown()
- Leerzeichen
- pensize ist nicht nötig, kann weggelassen werden
- for farbe in farben (statt i in farben, weil 1 immer eine Zahl ist, die langsam hoch zählt)
- counter: kann man gleich bei 110 anfangen und dann steigen lassen, weil er nie ohne die 100 verwendet wird
- statt „farben“ könnte auch „farbenliste“ verwendet werden, aber ist nur eine Kleinigkeit
- pendown könnte auch an den Anfang gestellt werden (oder an andere Positionen), ganz am Anfang braucht man pendown nicht, weil der Stift am Anfang immer unten ist, ebenso default wie pensize(1) → muss nicht dazu gesagt werden

→ verändert am Ergebnis nichts, die ursprüngliche Version hat auch funktioniert, aber macht es so einfacher


### Farbverläufe


Anstelle von Wörtern, was könnte man den Farben geben?
```python
from turtle import *

shape(“urtle“)
pensize(10)

fd(50)

pencolor(0.5, 0.5, 0.5)  → wird ein dicker Strich, turtle ist grau umrandet

pencolor(1, 0.5, 0.5) → turtle rosa umrandet

pencolor(0.2, 0.9, 0.5)
pencolor (r,    g,    b   ) → bisschen rot, viel grün, mittelviel blau, muss unter 1 sein
```


Aufgabe: Soll ganz langsam zu einem intensiven Rot werden


```
pencolor(0, 0, 0)
pencolor(0.1, 0, 0)
pencolor(0.2, 0, 0)
```

→ so in 20 Schritten bis zu (0.99, 0, 0)



schrittanzahl = 10

for i in range(10): → geht dann immer um 1/10 hoch
	pencolor(i/schrittanzahl, 0, 0)






i ist irgendwas, schrittanzahl ist irgendwas, i durch schrittanzal (i/schrittanzahl) ist auch irgendwas

→ bei i = i ist i/schrittanzahl 0.1, bei i=4 ist es 0,5 → Tabelle anlegen hilft dabei, Zusammenhänge zu sehen
→ statt Tabelle selbst anzulegen kann man sie auch machen lassen: 
	print(„i:“, „schrittanzahl“, „i/schrittanzahl“)
→ wenn mann das mit 20 durchlaufen lässt, kriegt man den Farbverlauf raus :) 



## Pikos Dateien
```python
import random
from math import sqrt

sqrt(16)


zahl = random.randint(1, 6)
print(zahl * 4)


liste = ["Äpfel", "Tomaten", "Mozartkugeln", "Kekse", "Limonade"]
random.choice(("Äpfel", "Tomaten", "Mozartkugeln", "Kekse", "Limonade"))

sache = random.choice(liste)
print(sache)
```

```python
from turtle import *

shape("turtle")
pensize(10)

fd(50)

pencolor(0.2, 0.9, 0.5)
        #r   g    b
# 		(0,   0,   0)
#		(0.1, 0,   0) circa!
# 		(0.2, 0,   0) circa!
#      ....
#		(0.99, 0,   0) circa!
fd(50)


schrittanzahl = 20

for i in range(schrittanzahl):
    print("i:", i, "i/schr.", i/schrittanzahl)
    pencolor(1-(i/schrittanzahl), 0, 0)
    forward(10)
```
```
i  		schrittanzahl		i/schrittanzahl
0		10						0
1								0.1
2								0.2
3								0.3
4
5
6
```