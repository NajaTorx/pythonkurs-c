# Pikos Python Kurs am 8.12.2022

Feiertage: 22.12. findet ntatt, 29.12. fällt aus, es gibt extra Hausaufgaben


## Pads und Links
Infos von letzter Stunde
https://md.ha.si/s/h9wZTQe7G#


Tiere/Wörter mit C (gerne weitersammeln!):
https://md.ha.si/gbPrE20mTpuPrl_vsmrzww#


### Videokonferenzmöglichkeiten
meet.jit.si

senfcall (kostenlose BBB Instanzen): https://www.senfcall.de/
bbb.daten.reisen



# Pythonkurs Protokoll 8. Dez

## Infos
Alle Infos, auch spontane Absagen gibts bei gitlab. Treffen können auch stattfinden, wenn das piko mal nicht kann, zum HA's besprechen zb.

Bitte keine Absagemails wer nicht zum Kurs kommt, weil nicht nötig (:

29.12. fällt aus 

## Hausaufgaben 
üüüben, auch über die feiertage - bringts voll. 

.0 Zahlen sind floats, keine integers. "floating point number". test: type(8) oder type(8.0)
>> Wenns geht, nimmt eins integers. manchmal macht python floats draus

Achtung, das deutsche Komma wird zum Punkt. 

### Lerntipp: 
Gute Methode zum lernen: Sachen ausprobieren und erstmal versuchen zu verstehen, wie es funktioniert. zb Aufgabe 2 vom letzten mal. Und immer die Fehlermeldungen lesen! 

### turtle
from turtle import *
Startet Malumgebung. 

help() gibt hilfe für das, was in klammern steht. 

Fremder Code: ist der code vertrauenswürdig? dann kann eins ausprobieren was er macht. 

## Thema heute: Variablen
Variablen sind wie beschriftete Kabelkisten: nur dass Python nicht checkt wenn der Inhalt nicht zu der Beschriftung passt. In Python kann eins also auch mit Variablen (=Kisten) rechnen statt mit Zahlen. Das gute: der Inhalt der Kisten kann sich ändern, Python rechnet trotzdem. 

Zahlen werden ohne, Buchstaben mit Gänsefüßchen angegeben. So können Wörter in Variablen gespeichert werden. 

Es wäre gut, wenn die Beschriftung auch zum Kisteninhalt passt. 

shape('turtle') macht alles cuter

## Kommentieren 
\# diese Zeile ist nun auskommentiert, dh. Python überliest sie.
Mehrere Zeilen auskommentieren: markieren und STRG + 3


## Pikos Dateien

Turtle-Funktionen:
```
forward() und backward()
left() und right()
goto()
undo(), clear() und reset()
circle()
penup() und pendown()
color()
pensize()

später: 
speed()
shape("turtle")
```
Kommandozeile:
```
Python 3.10.8 (/usr/bin/python)
>>> 2 * 4
8
>>> 2.0 * 4
8.0
>>> type(8)
<class 'int'>
>>> type(8.0)
<class 'float'>
>>> 2,0 * 4
(2, 0)
>>> 2 , 0*4
(2, 0)
>>> 2                 ,4
(2, 4)
>>> 4 / 2
2.0
>>> from turtle import *
>>> forward(100)
>>> rt(60)
>>> forward(100)
>>> rt(60)
>>> forward(100)
>>> rt(60)
>>> forward(100)
>>> forward(100)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 5, in forward
turtle.Terminator
>>> forward(100)
>>> forward(100)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 5, in forward
turtle.Terminator
>>> forward(100)
>>> right(60)
>>> forward(100)
>>> right(60)
>>> forward(100)
>>> right(60)
>>> forward(100)
>>> circle()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: circle() missing 1 required positional argument: 'radius'
>>> circle(100)
>>> circe()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'circe' is not defined
>>> forward(100)
>>> colour("red")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'colour' is not defined
>>> color("red")
>>> forward(100)
>>> pensize(20)
>>> foward(100)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'foward' is not defined
>>> fd(100)
>>> rt(90)
>>> width(5)
>>> fd(200)
>>> help(width)
Help on function width in module turtle:

width(width=None)
    Set or return the line thickness.
    
    Aliases:  pensize | width
    
    Argument:
    width -- positive number
    
    Set the line thickness to width or return it. If resizemode is set
    to "auto" and turtleshape is a polygon, that polygon is drawn with
    the same line thickness. If no argument is given, current pensize
    is returned.
    
    Example:
    >>> pensize()
    1
    >>> pensize(10)   # from here on lines of width 10 are drawn

>>> help(print)
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
    
    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.

>>> 




>>> %Run -c $EDITOR_CONTENT
>>> zahl1 = 8
>>> zahl1
8
>>> zahl1 + 2
10
>>> zahl2 = 6
>>> zahl1 + zahl2
14
>>> pensize(zahl2)
>>> forward(100)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 5, in forward
turtle.Terminator
>>> forward(100)
>>> pensize(zahl2)
>>> forward(100)
>>> pensize(zahl1 + zahl2*5)
>>> forward(100)
>>> coords()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'coords' is not defined
>>> coord()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'coord' is not defined
>>> zahl1
8
>>> zahl1 = 9
>>> zahl1
9
>>> zahl1 = "red"
>>> pencolor("blue")
>>> farbe1 = "blue"
>>> farbe2 = "red"
>>> pencolor(farbe1)
>>> forward(100)
>>> farbe3 = farbe1
>>> farbe4 = farbe 3
  File "<stdin>", line 1
    farbe4 = farbe 3
                   ^
SyntaxError: invalid syntax
>>> farbe4 = farbe3
>>> farbe3 = farbe2
>>> pencolor(farbe4)
>>> forward(100)
>>> position()
(500.00,0.00)
>>> goto(0, 500)
>>> reset()
>>> shape("turtle")
>>> size(20)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'size' is not defined
>>> right(10)
>>> rt(10)
>>> left(20)
>>> fd(20)
>>> fd(200)
>>> fd(100)
>>> undo()
>>> clear()
>>> reset()
>>> penup()
>>> fd(100)
>>> pendown()
>>> fd(100)
>>> penup()
```


```python
from turtle import *

for i in range(8):
    forward(100)
    left(45)
```

```python
from turtle import *

for knallgeraeusch in range(6):
    forward(100)
    penup()
    forward(10)
    pendown()
```

```python
from turtle import *

speed(0)

pensize(3)
for knallgeraeusch in range(20):
    circle(100)  # Malt einen Kreis mit Radius 100
    penup()
    forward(15)
    pendown()
    #print("hallo")
```
