# Pikos Python Kurs am 15.12.2022

## Pads und Links
Schöne Kreise: https://md.ha.si/sQHFY5kiRV-WPRii3DwX-A#  
Codeschnipsel: https://md.ha.si/F9F0Dka8SZO03BUqMVV7Gg?both
tl;draw: https://www.tldraw.com/r/1670523189721

Veranstaltung zwischen den Jahren: https://pretalx.c3voc.de/fire-shonks-2022/schedule/
Pikos Internet: https://chaos.social/@piko/109523958939858174

## Protokoll

neuer Name der Gruppe: Clownfisch

# Benennung des Codes einer Schleife

1. Zeile: `for i in range():` = Schleifenkopf

Zeile 2, 3 ... = Schleifenkörper (Ausführung der Schleife)


# Code, um im Script leere Zeile einzufügen
print()


### Codeschnipsel
```
text = „Hallo Pythonistas!“ # schräge Anführungszeichen oder Anführungszeichen „“ werden nicht grün, d.h. sie werden nicht als String erkannt

text = "Hallo Pythonistas!" # gerade Anführungszeichen werden grün, d.h. sie werden als String erkannt
```
Zum Unterschied zwischen doppelten Anführungszeichen und einfachem Anführungszeichen/Apostroph kommen wir noch


### Farbenbezeichnungen suchen über Begriffe
python 
turtle (damit die Farben des Turtle-Moduls angezeigt werden und nicht irgendwelche anderen, die eins mit python verwneden kann)
colors 
explanation (wenn Erklärung gewünscht ist)


### Hausaufgabenbesprechung
Aufgabe 1.1: in der Kommandozeile eine Variable als String definieren und Stringvariable mit Integer multiplizieren

wort = "wald" # Variable (wort) definiert String ("wald")
3 * wort 

Ergebnis: waldwaldwald


# hallo-Pyramide:
in der Ausführung einer for-Schleife einen String mit einer Integervariable multiplizieren
```
for i in range(8):
	print("hallo" * i)
```
Ergebnis: hallo-Pyramide



Variation von Aufgabe 2.1: verschachtelte for-Schleife 
```
for i in range(8):
	for i in range(8):
		print(i)
```
Ergebnis: 7 x 0-7 (7x7=49)


weitere Variation der for-Schleife:
```
for i in range(10): 
	# i = 0, i = 1, i = 2,… 
	print(i)
	i = 5 # jede 2. Zahl ist 5
	print(i)
```
Ergebnis: 0, 5, 1, 5, 2, 5, … , 9, 5 keine verschachtelte Schleife!


# Aufgabe 2.3 Regenbogen: Variable aus mehreren Strings definieren und in for-Schleife gestalten
```
from turtle import *

farben = ("red“, "organge“, "yellow“, "green“, "blue“, "darkMagenta“) # Variable definieren

pensize(50) # Stiftbreite definieren
for farbe in farben: # for Schleife definieren
	color(farbe) # color Variable farbe zuweisen

forward(100)
forward(-100) # alternativ backward(100)

right(90) 
forward(50) # wegen pensize 50 auch hier 50
left(90) # Fahne malen – Streifen auf linker Seite einen kleinen Strich nach unten -  mit penup und pendown eleganter (wie auf rechter Seite abgerundete Streifen)
```



unterschiedliche Aufzählungsweisen von range (nur Integer, keiner Strings!)
```
for i in range(3): # range(Integer) ist Angabe wie oft Schleife wiederholt wird – 1
	print(i)

# Ergebnis: 0 1 2 (untereinander)

# das Gleiche wie:

for i in range(0, 3, 1): # kein Tupel! Integer 0 = Beginn, Integer 3 = Ende+1, Integer 1 ist Angabe der Schrittfolge – wenn Schrittfolge 1 ist, dann kann dieser Integer auch weggelassen werden
	print(i)
# Ergebnis: 0, 1, 2 (untereinander) 

# fast das Gleiche wie:

for i in (0, 1, 2): # Tupel aus Integer
# Ergebnis: 0, 1, 2 (untereinander)
```

Tuple = runde Klammer (Tuple besteht entweder aus Integer oder aus String)
```
for i in ("red", "green", "blue"): # Tuple besteht aus 3 Strings
	print(i) 

# das Gleiche wie:

farbentupel = ("red", "green", "blue") # Variable ausgelagert aus for-Schleife
for i in farbentupel:
	print(i)


Liste = eckige Klammer (Liste besteht entweder aus Integer oder aus String)

for i in [0, 1, 2]
	print(i)

# das Gleiche wie:

zahlenliste = [0, 1, 2] # Liste als Variable aus for-Schleife auslagern (wie bei Tupel)
for i in zahlenliste:
	print(i)
```










Übung: in Kommandozeile zwei Variablen definieren und zwei Strings miteinander addieren
```
wort1 = "ha"
wort2 = "llo"
wort1 + wort2
# Ergebnis: hallo
```

Begrüßungsautomat: Kombination von Addition: String + String + String und Multiplikation: String * Integer 
```
personenliste = ["James", "Jean-Luc", "Kathryn", "Benjamin", "Jonathan"] 
# Liste besteht aus 5 Strings, die in dem Fall Namen sind

counter = 1 # counter wird als  Variable aus einem Integer definiert
# Variable besteht aus Integer

for person in personenliste:
    begruessung = "Hallo, " * counter + person + "!" 
    # Multiplikation zwischen String und Integer sowie Addition von zwei Strings
     print(begruessung)  

    print("Dies war Begrüßung Nummer " + str(counter)) 
    # Addition von String mit Integer, der in String umgewandelt wird
```


## Pikos Dateien
Hausaufgabenbesprechung
```python
# for i in range(8):
#     for j in range(3):
#         print(i)
#     
for i in range(8):
    print("hallo" * i)

print()

for i in range(0, 25, 4):
    print(i)
```
```python
from turtle import *

farben = ("red", "orange", "yellow", "green", "blue", "DarkMagenta")
pensize(50)
for farbe in farben:
    color(farbe)
    forward(100)
    forward(-100)
    rt(90)
    forward(50)
    left(90)
```
for-Schleifen
```python
for i in range(10):
    # i = 0, i = 1, i = 2, ...
    print(i)
    i = 5
    print(i)
```

```python
from turtle import *
for farbe in ("red", "green", "blue"):
    color(farbe)
    forward(100)
    left(120)

# gleichwertig, nur in eine Variable ausgelagert:

farbentupel = ("red", "green", "blue")
for farbe in farbentupel:
    color(farbe)
    forward(100)
    left(120)

```

Was tut range()?
```python
for farbe in ("red", "green", "blue"):
    print(farbe)
    
for i in [0, 1, 2]:
    print(i)

for i in range(3):  # range(3) ist fast das Gleiche wie (0, 1, 2)
    print(i)

zahlenliste = [0, 1, 2] 
for i in zahlenliste:
    print(i)
```
Personenbegrüßungsautomat
```python
# Begrüßungsautomat
# wort1 = "ha"
# wort2 = "llo"
# wort1 + wort2 # fügt die beiden strings zusammen

personenliste = ["James", "Jean-Luc", "Kathryn", "Benjamin", "Jonathan"]
counter = 1

for person in personenliste:
    begruessung = "Hallo, " * counter + person + "!"
    print(begruessung)
    print("Dies war Begrüßung Nummer " + str(counter))
    counter = counter + 1
```













